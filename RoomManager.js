const Room = require('./Room');

const roomTemplates = [];

module.exports = () => {
  // mapping of all available rooms
  const rooms = new Map(
    roomTemplates.map((c) => [
      c.name,
      Room(c),
    ]),
  );

  function createRoom(name) {
    rooms.set(name, Room({ name }));
  }

  function removeClient(client) {
    rooms.forEach((c) => c.removeUser(client));
  }

  function getRoomByName(roomName) {
    return rooms.get(roomName);
  }

  function serializeRooms() {
    console.log(Array.from(rooms.values()).map((c) => c.serialize()));
    return Array.from(rooms.values()).map((c) => c.serialize());
  }

  return {
    removeClient,
    getRoomByName,
    serializeRooms,
    createRoom,
  };
};
