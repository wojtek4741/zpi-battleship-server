const express = require('express');

// const app = express();
const socketIO = require('socket.io');
const server = require('http').createServer(express);
// const bodyParser = require('body-parser');
// const db = require('./database.js');

// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

const { Pool } = require('pg');

const pool = new Pool({
  user: 'ndrxzckpjfsqhn',
  host: 'ec2-46-137-79-235.eu-west-1.compute.amazonaws.com',
  database: 'dccv24jbjivcam',
  password: '0642a378e7da54cb45b3c82ac68b6f2f5d62fc50b163284e5532d211b62664fb',
  port: 5432,
  ssl: { rejectUnauthorized: false },
});

console.log('NODE_ENV', process.env.NODE_ENV);

// const HTTP_PORT = 8000;
const port = process.env.PORT || 3001;

const io = socketIO(server);

const ClientManager = require('./ClientManager');
const RoomManager = require('./RoomManager');
const makeHandlers = require('./handlers');

const clientManager = ClientManager();
const roomManager = RoomManager();

io.on('connection', (client) => {
  const {
    handleRegister,
    handleJoin,
    handleLeave,
    handleMessage,
    handleGetRooms,
    handleGetAvailableUsers,
    handleDisconnect,
    handleNewUser,
    handleCreateRoom,
    handleBoard,
    handleTorpedo,
  } = makeHandlers(client, clientManager, roomManager, pool);

  console.log('client connected...', client.id);
  clientManager.addClient(client);

  client.on('newuser', handleNewUser);
  client.on('createroom', handleCreateRoom);
  client.on('register', handleRegister);
  client.on('join', handleJoin);
  client.on('fireTorpedo', handleTorpedo);
  client.on('leave', handleLeave);
  client.on('message', handleMessage);
  client.on('setUserBoard', handleBoard);
  client.on('rooms', handleGetRooms);
  client.on('availableUsers', handleGetAvailableUsers);
  client.on('disconnect', () => {
    console.log('client disconnect...', client.id);
    handleDisconnect();
  });
  client.on('error', (err) => {
    console.log('received error from client:', client.id);
    console.log(err);
  });
});

server.listen(port, () => console.log(`Socket server listening on port ${port}`));

// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
//   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//   next();
// });

// app.listen(HTTP_PORT, () => {
//   console.log('API running on port %PORT%'.replace('%PORT%', HTTP_PORT));
// });

// app.get('/test', (req, res) => {
//   res.json({ message: 'Ok' });
// });

// Global Ranking
// app.get('/api/users/', (req, res) => {
//   const sql = 'SELECT substr(email, 1, instr(email, \'@\')-1) nick, points FROM user ORDER BY 2 DESC';
//   const params = [];
//   db.all(sql, params, (err, rows) => {
//     if (err) {
//       res.status(400).json({ error: err.message });
//       return;
//     }
//     res.json(rows);
//   });
// });

// User Rank
// app.get('/api/user/', (req, res) => {
//   const sql = 'SELECT substr(email, 1, instr(email, \'@\')-1) nick, points FROM user WHERE email = ?';
//   const email = [req.query.email];
//   db.get(sql, email, (err, row) => {
//     if (err) {
//       res.status(400).json({ error: err.message });
//       return;
//     }
//     res.json(row);
//   });
//   // next();
// });

/*
// User Battle History
app.get('/api/user/', (req, res) => {
  const sql = `SELECT substr(player1, 1, instr(player1, '@')-1) player1,
              substr(player2, 1, instr(player2, '@')-1) player2,
              player1_rank, player2_rank, player1_points, player2_points,
              CASE winner
                WHEN 0
                  THEN substr(player1, 1, instr(player1, '@')-1)
                ELSE substr(player2, 1, instr(player2, '@')-1)
              END winner,
              time, player1_hits, player2_hits, player1_misses, player2_misses,
              player1_field, player2_field
              FROM battle WHERE player1 = ? OR player2 = ?`;
  const email = [req.query.email, req.query.email];
  db.all(sql, email, (err, rows) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json(rows);
  });
});
*/

// app.post('/api/user/', (req, res) => {
//   const errors = [];
//   console.log(req.body);
//   if (!req.body.email) {
//     errors.push('No email specified');
//   }
//   if (errors.length) {
//     res.status(400).json({ error: errors.join(',') });
//     return;
//   }
//   const data = {
//     email: req.body.email,
//     points: 0,
//   };
//   const sql = 'INSERT INTO user (email, points) VALUES (?,?)';
//   const params = [data.email, data.points];
//   db.run(sql, params, (err) => {
//     if (err) {
//       res.status(400).json({ error: err.message });
//       return;
//     }
//     res.json(data);
//   });
// });
